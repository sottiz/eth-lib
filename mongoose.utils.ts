import {Mongoose, connect} from "mongoose";


export class MongooseUtils {
    
    public static connect(): Promise<Mongoose> {
        return connect(process.env.MONGO_URI, {
            auth: {
                username: process.env.MONGO_USER,
                password: process.env.MONGO_PASSWORD
            },
            authSource: "admin",
            autoCreate: true
        });
    }

    public static isDuplicateKeyError(err: Error): boolean {
        return err.name === "MongoServerError" && err['code'] === 11000;
    }
}