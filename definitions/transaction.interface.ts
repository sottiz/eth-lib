import { IAccount } from "./account.interface";

export interface ITransaction {
    date: Date;
    amount: number;
    from?: string | IAccount;
    to?: string | IAccount;
}