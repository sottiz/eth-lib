import { IAccount } from "./account.interface";
import { ITransaction } from "./transaction.interface";

export interface IWallet {
    account: string | IAccount;
    name: string;
    transactions: string[] | ITransaction[];
}