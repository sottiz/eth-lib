export interface IAccount {
    username: string;
    email: string;
    password: string;
    birthDate: Date;
}