import { IAccount } from './account.interface';

export interface ISession {
    token: string;
    expirationDate?: Date;
    platform: string;
    account: string | IAccount;
}