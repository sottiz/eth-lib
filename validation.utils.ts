import { plainToInstance } from "class-transformer";
import { validate } from "class-validator";
import {Response} from 'express';

export class ValidationUtils {
  static async createAndValidateDTO<DTO extends object>(constructor: new (...args: any[]) => DTO, raw: object, res: Response): Promise<DTO | undefined> {
    
    const dto = plainToInstance(constructor, raw,{
        excludeExtraneousValues: true
    });

    const errors = await validate(dto);

    if(errors.length > 0) {
        const errorMessages: {[key: string]: any} = {};
        for(let validationError of errors) {
            errorMessages[validationError.property] = validationError.constraints;
        }
        res.status(400).json(errorMessages);
        return;
    }

    return dto;
  }
}